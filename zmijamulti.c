#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include <windows.h>

void crtajTablu (int i, int j, int matrix [i][j], int p1score, int p2score)
{
    printf("%9s%3d\n", "P2 score:", p1score);
    for (i=0; i<12; i++)
    {
        for (j=0; j<12; j++)
        {
            printf("%c", matrix[i][j]);
        }
        putchar('\n');
    }
    printf("%9s%3d\n", "P1 score:", p2score);
}

void START_SCREEN ()
{
    int i;

    for (i=0;i<5; i++)
    {
        putchar('\n');
    }

    printf("%58s", "*C BATTLE*");
    putchar('\n');
    printf("%58s", "*DA GAME!*");
    putchar('\n'); putchar('\n'); putchar('\n');
    printf("%59s", "PRESS ENTER\n");
    getch();
    system ("cls");
}

void DLC ()
{
/*To be implemented*/
}

int main(){

    int i, j, k, l, p, t, x1=1, y1=0, x2=0, y2=-1;
    int eaten=1, score=0, p1score=0, p2score=0, p1input=1, p2input=1;
    char p1='1', p2='2';
    char ch, unosp1, unosp2;
    int matrix[12][12];
    DWORD start, check;

    for (i=0; i<12; i++)
    {
        for (j=0; j<12; j++)
        {
            if (i==1&&j==1)
            {
                matrix[i][j]=p1;
                continue;
            }
            if (i==10&&j==10)
            {
                matrix[i][j]=p2;
                continue;
            }
            (i==0||i==11||j==0||j==11)?(matrix[i][j]='/'):(matrix[i][j]=' ');
        }
    }

    START_SCREEN ();

    //DLC ();

    printf ("A - levo, D - desno, W - gore, S - dole, K za kraj\n\n");

    crtajTablu(12, 12, matrix, p1score, p2score);

    i=1, j=1, k=10, l=10;       //startne lokacije igraca;

    while(((i>0&&i<11)&&(j>0&&j<11))&&((k>0&&k<11)&&(l>0&&l<11)))
    {

        if (eaten)
        {
            while (1)
            {
                //generise random koordinate za O, koordinate su u rasponu od 1 do 10
                if (((p=rand()%11)&&p!=i&&p!=j&&p!=k&&p!=l)&&((t=rand()%11)&&t!=i&&t!=j&&t!=k&&t!=l))
                {
                    matrix [p][t]='O';
                    eaten=0;
                    break;
                }
                else
                {
                    continue;
                }
            }
        }

        start=GetTickCount();
        check=start+250;        //Sto manji interval stavis, to ce zmija biti brza

        putchar('\n');



        while ((check>GetTickCount())/*&&(!p1input||!p2input)*/)
        {
            if (kbhit())
            {
                ch=getch();
                fflush(stdin);
                if ((ch=='A'||ch=='S'||ch=='D'||ch=='W'||ch=='a'||ch=='s'||ch=='d'||ch=='w')&&!p1input)
                {
                    unosp1=ch;
                    p1input=1;
                }
                else if ((ch=='I'||ch=='J'||ch=='K'||ch=='L'||ch=='i'||ch=='j'||ch=='k'||ch=='l')&&!p2input)
                {
                    unosp2=ch;
                    p2input=1;
                }
            }
            p1input=0, p2input=0;
        }
        fflush(stdin);



        switch(unosp1)
        {
            case 'a':
            case 'A': x1=-1, y1=0; break;
            case 'd':
            case 'D': x1= 1, y1=0; break;
            case 'w':
            case 'W': y1=-1, x1=0; break;
            case 's':
            case 'S': y1= 1, x1=0; break;
            case 'y':
            case 'Y': system ("cls"); printf("\n\nENDE!\n\nSCORE:%3d\n", score); return 0;
        }

        switch(unosp2)
        {
            case 'j':
            case 'J': x2=-1, y2=0; break;
            case 'l':
            case 'L': x2= 1, y2=0; break;
            case 'i':
            case 'I': y2=-1, x2=0; break;
            case 'k':
            case 'K': y2= 1, x2=0; break;
            case 'y':
            case 'Y': system ("cls"); printf("\n\nENDE!\n\nSCORE:%3d\n", score); return 0;
        }

        matrix[i][j]=' ';
        matrix[k][l]=' ';
        matrix[i+y1][j+x1]=p1;
        matrix[k+y2][l+x2]=p2;

        i=i+y1;
        j=j+x1;
        k=k+y2;
        l=l+x2;

        if (((i==p)&&(j==t))||((k==p)&&(l==t)))
        {
            eaten=1;
            ((i==p)&&(j==t))?p1score++:(((k==p)&&(l==t))?p2score++:5);

        }

        system ("cls");

        crtajTablu(12, 12, matrix, p1score, p2score);

    }

    system ("cls");
    printf("\n\nGAME OVER!\n\nP1 SCORE:%3d\nP2 SCORE:%3d\n", p1score, p2score);
    getch();


    return 0;
}
